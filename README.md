#Vascomm Test

##Installation

1. Copy .env.example to .env `cp .env.example .env`
2. Configure database connection in `.env` file
3. Run `composer install`
4. Run `php artisan migrate --seed`
5. Run `php artisan serve`

##Logging In

There are 2 initial users:
1. Admin Role
   - Email: `admin@admin.com`
   - Password: `password`

2. Guest Role
   - Email: `guest@guest.com`
   - Password: `password`

##Warning

Don't forget to run the backend
