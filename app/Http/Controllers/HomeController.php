<?php

namespace App\Http\Controllers;

use App\Content;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $contents = Http::get(env('BACKEND_URL') . '/contents');

        if ($contents->status() == 200 && $contents->body() != '') {
            $contents = json_decode($contents->body());
        } else {
            abort(500, 'Error');
        }

        return view('home', compact('contents'));
    }

    public function view($slug)
    {
        $content = Http::get(env('BACKEND_URL') . '/contents/' . $slug);

        if ($content->status() == 200 && $content->body() != '') {
            $content = json_decode($content->body());
        } else {
            abort(500);
        }

        return view('view', compact('content'));
    }

    public function create()
    {
        return view('create');
    }

    public function insert(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'body' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator->getMessageBag())
                ->withInput();
        }

        try {
            $content = Http::post(env('BACKEND_URL') . '/contents', [
                'title' => $request->input('title'),
                'body' => $request->input('body'),
            ]);

            if ($content->status() == 200) {
                $content = json_decode($content->getBody());
            } else {
                abort(500);
            }

            return redirect()->route('home')->with('success', 'New content added successfully.');
        } catch (\Exception $exception) {
            return redirect()
                ->back()
                ->withErrors($exception->getMessage())
                ->withInput();
        }
    }

    public function edit($slug)
    {
        $content = Http::get(env('BACKEND_URL') . '/contents/' . $slug);

        if ($content->status() == 200 && $content->body() != '') {
            $content = json_decode($content->body());
        } else {
            abort(500);
        }

        return view('edit', compact('content'));
    }

    public function update(Request $request, $slug)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'body' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator->getMessageBag())
                ->withInput();
        }

        try {
            $content = Http::put(env('BACKEND_URL') . '/contents/' . $slug, [
                'title' => $request->input('title'),
                'body' => $request->input('body'),
            ]);

            if ($content->status() == 200) {
                $content = json_decode($content->body());
            } else {
                abort(500, 'Error');
            }

            return redirect()->route('home')->with('success', 'Content updated successfully.');
        } catch (\Exception $exception) {
            return redirect()
                ->back()
                ->withErrors('Error')
                ->withInput();
        }
    }

    public function delete($slug)
    {
        $content = Http::delete(env('BACKEND_URL') . '/contents/' . $slug);

        if ($content->status() == 200) {
            return response()->json([
                'success' => true,
                'message' => 'Content removed successfully'
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Content removed failed'
            ], 500);
        }
    }
}
