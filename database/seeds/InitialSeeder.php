<?php

use Illuminate\Database\Seeder;

class InitialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        \Spatie\Permission\Models\Permission::create(['name' => 'view contents']);
        \Spatie\Permission\Models\Permission::create(['name' => 'edit contents']);
        \Spatie\Permission\Models\Permission::create(['name' => 'delete contents']);
        \Spatie\Permission\Models\Permission::create(['name' => 'create contents']);

        // create roles and assign existing permissions
        $role1 = \Spatie\Permission\Models\Role::create(['name' => 'admin']);
        $role1->givePermissionTo('view contents');
        $role1->givePermissionTo('edit contents');
        $role1->givePermissionTo('delete contents');
        $role1->givePermissionTo('create contents');

        $role2 = \Spatie\Permission\Models\Role::create(['name' => 'guest']);
        $role2->givePermissionTo('view contents');

        // create demo users
        $user = \App\User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('password'),
        ]);
        $user->assignRole($role1);

        $user = \App\User::create([
            'name' => 'Guest',
            'email' => 'guest@guest.com',
            'password' => bcrypt('password'),
        ]);
        $user->assignRole($role2);
    }
}
