@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-right">
                            <a href="{{ route('home') }}" class="btn btn-outline-secondary"><i
                                    class="fa fa-arrow-left"></i> Back to home</a>
                        </div>
                        New Content
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if($errors = session('errors'))
                            @foreach($errors as $error)
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endforeach
                        @endif

                        <form action="{{ route('update', ['slug'=> $content->slug]) }}" class="form" method="post">
                            {{ method_field('put') }}
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="title" class="form-label">Title</label>
                                <input type="text" name="title" class="form-control"
                                       value="{{ old('title', $content->title) }}"
                                       placeholder="Insert title ..." required>
                            </div>
                            <div class="form-group">
                                <label for="body" class="form-label">Content Body</label>
                                <textarea name="body" class="form-control" placeholder="Insert body content ..."
                                          required>{{ old('body', $content->body) }}</textarea>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Submit">
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
