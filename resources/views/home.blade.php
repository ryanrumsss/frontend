@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-right">
                            <a href="{{ route('create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> New
                                Content</a>
                        </div>
                        {{ __('Dashboard') }}
                    </div>

                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session('success') }}
                            </div>
                        @endif

                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead class="thead-dark text-center">
                                <tr>
                                    <th class="col-1">ID</th>
                                    <th class="col-4">Title</th>
                                    @hasrole('admin')
                                    <th class="col-2">Visit Counter</th>
                                    @endhasrole
                                    <th class="col-3">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($contents as $content)
                                    <tr>
                                        <th class="text-center">{{ $content->id }}</th>
                                        <td class="col-3">{{ $content->title }}</td>
                                        @hasrole('admin')
                                        <td>{{ number_format($content->visit_count, 0, ',', '.') }}</td>
                                        @endhasrole
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a href="{{ route('view', ['slug' => $content->slug]) }}"
                                                   class="btn btn-primary"><i class="fa fa-eye"></i> View</a>
                                                @hasrole('admin')
                                                <a href="{{ route('edit', ['slug' => $content->slug]) }}"
                                                   class="btn btn-secondary"><i class="fa fa-pen"></i> Edit</a>
                                                <a class="btn btn-danger" data-toggle="modal"
                                                   data-target="#modalDelete{{$content->id}}"><i
                                                        class="fa fa-close"></i>
                                                    Delete</a>
                                                @endhasrole
                                            </div>
                                        </td>
                                    </tr>

                                    <div class="modal fade" id="modalDelete{{ $content->id }}"
                                         data-backdrop="static"
                                         tabindex="-1" role="dialog" aria-labelledby="modalDelete"
                                         aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Delete Confirmation</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Are you sure you want to delete content {{ $content->title }}?</p>
                                                </div>
                                                <div class="modal-footer">
{{--                                                    <a href="{{ route('delete', ['slug' => $content->slug]) }}" class="btn btn-primary">Confirm</a>--}}
                                                    <button type="button" class="btn btn-primary delete" data-id="{{ $content->slug }}">Confirm</button>
                                                    <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                    <tr>
                                        <td colspan="4" class="text-center">Empty content</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function (){
            $('.delete').click(function () {
                var slug = $(this).data('id');
                var token = '{{ csrf_token() }}';
                console.log(token);

                $.ajax({
                    url: '{{ env('APP_URL') }}' + '/content/' + slug,
                    type: 'POST',
                    data: {
                        "_token": token,
                        "_method": 'DELETE',
                    },
                    success: function (response) {
                        if (response.success) {
                            location.reload()
                        } else {
                            alert(response.message);
                        }
                    }
                })
            })
        })
    </script>
@endsection
