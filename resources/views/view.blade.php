@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-right">
                            <a href="{{ route('home') }}" class="btn btn-outline-secondary"><i
                                    class="fa fa-arrow-left"></i> Back to home</a>
                        </div>
                        {{ $content->title }}
                    </div>

                    <div class="card-body">
                        {!! $content->body !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
