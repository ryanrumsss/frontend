<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/new', 'HomeController@create')->name('create');
Route::post('/new', 'HomeController@insert')->name('insert');
Route::get('/content/{slug}', 'HomeController@view')->name('view');
Route::get('/content/{slug}/edit', 'HomeController@edit')->name('edit');
Route::put('/content/{slug}', 'HomeController@update')->name('update');
Route::delete('/content/{slug}', 'HomeController@delete')->name('delete');
